## Libraries
- Kotlin, ViewModel, Retrofit, Mockk, Dagger, RxJava

## Architecture
Clean Architecture + MVVM.

### Modules
**1** - :app (Android module) 🤖

    depends on:
        - :entities
        - :usecases
        - :network
        - :repository
      
**2** - :network (Android module) 🤖

    depends on:
        - :entities
        - :repository

**3** - :repository (Kotlin only module) ☕
    
    depends on:
        - :entities
        - :usecases
        
**4** - :usecases (Kotlin only module) ☕
    
    depends on:
        - :entities
   
**5** - :entities (Kotlin only module) ☕

## Tests
:app

    - Unit tests
    - Espresso tests

:repository
    
    - Unit tests  
    
: usecases
    
    - Unit tests

### TODO
Add tests for Sort and Filter
