package com.pp.usecases

import com.pp.CompanyModel
import com.pp.LaunchModel
import com.pp.usecases.MissionsListUseCase.Params
import com.pp.usecases.MissionsListUseCase.SpaceResponse.Data
import com.pp.usecases.MissionsListUseCase.SpaceResponse.Error
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers.trampoline
import org.junit.Test

class MissionsListUseCaseTest {
    private val params = mockk<Params>(relaxed = true) {
        every { sortingOrder } returns SortingOrder.ASCENDING
        every { year } returns null
    }
    private val firstLaunchesItem = mockk<LaunchModel>(relaxed = true) {
        every { launchDateUnix } returns 0
        every { year } returns YEAR
    }
    private val secondLaunchesItem = mockk<LaunchModel>(relaxed = true) {
        every { launchDateUnix } returns 1
        every { year } returns TEXT
    }
    private val companyInfo: CompanyModel = mockk(relaxed = true)
    private val repository: Repository = mockk(relaxed = true) {
        every { getCompanyInfo() } returns Observable.just(companyInfo)
        every { getLaunches() } returns Observable.just(
            listOf(
                firstLaunchesItem,
                secondLaunchesItem
            )
        )
    }
    private val useCase = MissionsListUseCase(repository, trampoline(), trampoline())

    @Test
    fun `when there is a valid response then returns the data`() {
        val testObserver = useCase.execute(params).test()

        testObserver.assertValue(Data(companyInfo, listOf(firstLaunchesItem, secondLaunchesItem)))
    }

    @Test
    fun `when there is an error then returns error`() {
        every { repository.getCompanyInfo() } returns Observable.error(Exception(MESSAGE))

        val useCase = MissionsListUseCase(repository, trampoline(), trampoline())
        val testObserver = useCase.execute(mockk(relaxed = true)).test()

        testObserver.assertValue(Error(MESSAGE))
    }

    @Test
    fun `when params contains year then filters`() {
        every { firstLaunchesItem.year } returns TEXT
        val testObserver = useCase.execute(
            mockk(relaxed = true) {
                every { year } returns YEAR
            }
        ).test()

        testObserver.assertValue(Data(companyInfo, emptyList()))
    }

    @Test
    fun `when order descending then sorts`() {
        every { params.sortingOrder } returns SortingOrder.DESCENDING

        val testObserver = useCase.execute(params).test()

        testObserver.assertValue(Data(companyInfo, listOf(secondLaunchesItem, firstLaunchesItem)))
    }

    private companion object {
        private const val MESSAGE = "message"
        private const val YEAR = "year"
        private const val TEXT = "text"
    }
}
