package com.pp.usecases

import io.reactivex.Observable

interface UseCase<Response, Params> {

    fun execute(params: Params): Observable<Response>
}
