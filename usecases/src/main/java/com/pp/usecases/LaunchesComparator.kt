package com.pp.usecases

import com.pp.LaunchModel

class LaunchesComparator(private val sortingOrder: SortingOrder) :
    Comparator<LaunchModel> {

    override fun compare(launchesItem0: LaunchModel, launchesItem1: LaunchModel): Int {
        val compared = launchesItem0.launchDateUnix - launchesItem1.launchDateUnix
        return if (sortingOrder == SortingOrder.ASCENDING) {
            compared
        } else {
            -compared
        }
    }
}
