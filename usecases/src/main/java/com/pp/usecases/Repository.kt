package com.pp.usecases

import com.pp.CompanyModel
import com.pp.LaunchModel
import io.reactivex.Observable

interface Repository {

    fun getCompanyInfo(): Observable<CompanyModel>

    fun getLaunches(): Observable<List<LaunchModel>>
}
