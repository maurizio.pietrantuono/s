package com.pp.repository

import com.pp.network.CompanyInfo
import com.pp.network.LaunchesItem
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test

class MemoryRepositoryTest {
    private lateinit var repository: MemoryRepository
    private val launch: LaunchesItem = mockk(relaxed = true)
    private val companyInfo: CompanyInfo = mockk(relaxed = true)
    private val client: SpaceClient = mockk(relaxed = true) {
        every { getCompanyInfo() } returns Observable.just(companyInfo)
        every { getLaunches() } returns Observable.just(listOf(launch))
    }

    @Before
    fun setUp() {
        repository = MemoryRepository(client)
    }

    @Test
    fun `when company info is not cached the calls the client`() {
        repository.getCompanyInfo().test()

        verify { client.getCompanyInfo() }
    }

    @Test
    fun `when launches info are not cached the calls the client`() {
        repository.getLaunches().test()

        verify { client.getLaunches() }
    }

    @Test
    fun `when company info is cached does not call the client`() {
        repository.getCompanyInfo().test()
        repository.getCompanyInfo().test()

        verify(exactly = 1) { client.getCompanyInfo() }
    }

    @Test
    fun `when launches are cached does not call the client`() {
        repository.getLaunches().test()
        repository.getLaunches().test()

        verify(exactly = 1) { client.getLaunches() }
    }
}
