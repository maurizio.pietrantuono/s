package com.pp.repository

import com.pp.CompanyModel
import com.pp.network.CompanyInfo

object CompanyInfoToCompanyModel : (CompanyInfo) -> CompanyModel {

    override fun invoke(companyInfo: CompanyInfo): CompanyModel =
        CompanyModel(
            name = companyInfo.name,
            founder = companyInfo.founder,
            founded = companyInfo.founded.toString(),
            employees = companyInfo.employees,
            launchSites = companyInfo.launch_sites,
            valuation = companyInfo.valuation,
        )
}
