package com.pp.repository

import com.pp.network.CompanyInfo
import com.pp.network.LaunchesItem
import io.reactivex.Observable

interface SpaceClient {

    fun getCompanyInfo(): Observable<CompanyInfo>

    fun getLaunches(): Observable<List<LaunchesItem>>
}
