package com.pp.repository

import com.pp.LaunchModel
import com.pp.network.LaunchesItem

object LaunchesItemToLaunchModel : (LaunchesItem) -> LaunchModel {
    override fun invoke(launchesItem: LaunchesItem): LaunchModel =
        LaunchModel(
            flightNumber = launchesItem.flight_number,
            missionName = launchesItem.mission_name,
            launchDateUnix = launchesItem.launch_date_unix,
            rocketName = launchesItem.rocket.rocket_name,
            rocketType = launchesItem.rocket.rocket_type,
            imageUrl = launchesItem.links.mission_patch_small,
            successful = launchesItem.launch_success,
            year = launchesItem.launch_year,
            articleLink = launchesItem.links.article_link,
            wikipediaLink = launchesItem.links.wikipedia,
            videoLink = launchesItem.links.video_link
        )
}
