package com.pp.repository

import com.pp.CompanyModel
import com.pp.LaunchModel
import com.pp.network.CompanyInfo
import com.pp.network.LaunchesItem
import com.pp.usecases.Repository
import io.reactivex.Observable

class MemoryRepository(
    private val spaceClient: SpaceClient,
    private val companyInfoToCompanyModel: (CompanyInfo) -> CompanyModel = CompanyInfoToCompanyModel,
    private val launchesItemToLaunchModel: (LaunchesItem) -> LaunchModel = LaunchesItemToLaunchModel
) : Repository {
    private var companyInfo: CompanyModel? = null
    private var launches: List<LaunchModel>? = null

    override fun getCompanyInfo(): Observable<CompanyModel> =
        companyInfo?.let { Observable.just(it) } ?: spaceClient.getCompanyInfo()
            .map { companyInfoToCompanyModel(it) }.doOnNext {
                companyInfo = it
            }

    override fun getLaunches(): Observable<List<LaunchModel>> =
        launches?.let { Observable.just(it) } ?: spaceClient.getLaunches()
            .flatMapIterable { it }
            .map { launchesItemToLaunchModel(it) }
            .toList()
            .toObservable()
            .doOnNext {
                launches = it
            }
}
