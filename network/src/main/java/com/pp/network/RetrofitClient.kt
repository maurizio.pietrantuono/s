package com.pp.network

import com.pp.repository.SpaceClient
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient : SpaceClient {

    private val retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()

    private val api = retrofit.create(SpaceApi::class.java)

    override fun getCompanyInfo(): Observable<CompanyInfo> = api.getCompanyInfo()

    override fun getLaunches(): Observable<List<LaunchesItem>> = api.getLaunches()

    private companion object {
        private const val BASE_URL = "https://api.spacexdata.com"
    }
}
