package com.pp.network

import io.reactivex.Observable
import retrofit2.http.GET

interface SpaceApi {

    @GET("v3/info")
    fun getCompanyInfo(): Observable<CompanyInfo>

    @GET("v3/launches")
    fun getLaunches(): Observable<List<LaunchesItem>>
}
