package com.pp.space.utils

import androidx.lifecycle.ViewModelProvider
import com.pp.space.di.MissionsListViewModelFactoryProvider
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.space.sortandfilter.viewmodel.SortAndFilterDataSource
import com.pp.usecases.MissionsListUseCase
import com.pp.usecases.UseCase

class TestListViewModelFactoryProvider : MissionsListViewModelFactoryProvider {

    override fun getMissionsListViewModelFactory(
        spaceUseCase: UseCase<MissionsListUseCase.SpaceResponse, MissionsListUseCase.Params>,
        mapper: (MissionsListUseCase.SpaceResponse) -> SpaceViewState,
        sortAndFilterDataSource: SortAndFilterDataSource
    ): ViewModelProvider.Factory =
        TestMissionsListViewModelFactory()
}
