package com.pp.space.utils

import android.app.Application
import com.pp.space.di.ApplicationComponent
import com.pp.space.di.ApplicationComponentProvider

class TestApplication : Application(), ApplicationComponentProvider {
    override lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent =
            DaggerTestApplicationComponent.create()
    }
}
