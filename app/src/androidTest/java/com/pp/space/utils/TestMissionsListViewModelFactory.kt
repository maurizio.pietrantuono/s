package com.pp.space.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

internal class TestMissionsListViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = TestViewModel as T
}
