package com.pp.space.utils

import com.pp.space.di.ApplicationComponent
import dagger.Component

@Component(modules = [TestApplicationModule::class])
interface TestApplicationComponent : ApplicationComponent
