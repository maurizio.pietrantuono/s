package com.pp.space.utils

import com.pp.network.RetrofitClient
import com.pp.repository.MemoryRepository
import com.pp.repository.SpaceClient
import com.pp.space.di.MissionsListViewModelFactoryProvider
import com.pp.usecases.MissionsListUseCase
import com.pp.usecases.Repository
import com.pp.usecases.UseCase
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
object TestApplicationModule {

    @Provides
    fun provideLogger(): Logger = AndroidLogger()

    @Provides
    fun provideClient(): SpaceClient = RetrofitClient()

    @Provides
    fun provideRepository(spaceClient: SpaceClient): Repository = MemoryRepository(spaceClient)

    @Provides
    fun provideUseCase(repository: Repository): UseCase<MissionsListUseCase.SpaceResponse, MissionsListUseCase.Params> =
        MissionsListUseCase(repository, Schedulers.io(), AndroidSchedulers.mainThread())

    @Provides
    fun provideMissionsListViewModelFactoryProvider(): MissionsListViewModelFactoryProvider =
        TestListViewModelFactoryProvider()
}
