package com.pp.space.utils

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.TimeUnit

abstract class DisposableViewModel<ViewState, ViewAction, OneOffEvents> :
    ViewModel(), Consumer<ViewAction> {

    protected var disposable: Disposable? = null

    protected val viewStatesSubject: BehaviorSubject<ViewState> = BehaviorSubject.create()

    protected val oneOffEventsSubject: Subject<OneOffEvents> = PublishSubject.create()

    open val viewStates: Observable<ViewState>
        get() = viewStatesSubject

    open val oneOffEvents: Observable<OneOffEvents>
        get() = oneOffEventsSubject.throttleFirst(1, TimeUnit.SECONDS)

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }
}
