package com.pp.space.utils

import android.view.View

val Any?.exhaustive get() = Unit

var View.visible: Boolean
    set(value) {
        visibility = if (value) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
    get() = visibility == View.VISIBLE
