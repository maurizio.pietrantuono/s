package com.pp.space.di

import com.pp.repository.SpaceClient
import com.pp.space.utils.Logger
import com.pp.usecases.MissionsListUseCase
import com.pp.usecases.Repository
import com.pp.usecases.UseCase
import dagger.Component

@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun client(): SpaceClient

    fun repository(): Repository

    fun useCase(): UseCase<MissionsListUseCase.SpaceResponse, MissionsListUseCase.Params>

    fun missionsListViewModelFactoryProvider(): MissionsListViewModelFactoryProvider

    fun logger(): Logger
}

interface ApplicationComponentProvider {
    var applicationComponent: ApplicationComponent
}
