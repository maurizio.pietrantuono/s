package com.pp.space.di

import androidx.lifecycle.ViewModelProvider
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.space.sortandfilter.viewmodel.SortAndFilterDataSource
import com.pp.usecases.MissionsListUseCase
import com.pp.usecases.MissionsListUseCase.SpaceResponse
import com.pp.usecases.UseCase

class ProductionListViewModelFactoryProvider : MissionsListViewModelFactoryProvider {

    override fun getMissionsListViewModelFactory(
        spaceUseCase: UseCase<SpaceResponse, MissionsListUseCase.Params>,
        mapper: (SpaceResponse) -> SpaceViewState,
        sortAndFilterDataSource: SortAndFilterDataSource
    ): ViewModelProvider.Factory =
        ProductionMissionsListViewModelFactory(spaceUseCase, mapper, sortAndFilterDataSource)
}
