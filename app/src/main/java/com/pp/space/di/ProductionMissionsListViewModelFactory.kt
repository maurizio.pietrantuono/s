package com.pp.space.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pp.space.missionslist.viewmodel.MissionsListViewModel
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.space.sortandfilter.viewmodel.SortAndFilterDataSource
import com.pp.usecases.MissionsListUseCase.Params
import com.pp.usecases.MissionsListUseCase.SpaceResponse
import com.pp.usecases.UseCase

internal class ProductionMissionsListViewModelFactory(
    private val useCase: UseCase<SpaceResponse, Params>,
    val mapper: (SpaceResponse) -> SpaceViewState,
    private val dataSource: SortAndFilterDataSource
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        MissionsListViewModel(
            useCase,
            mapper,
            dataSource
        ) as T
}

interface MissionsListViewModelFactoryProvider {

    fun getMissionsListViewModelFactory(
        spaceUseCase: UseCase<SpaceResponse, Params>,
        mapper: (SpaceResponse,) -> SpaceViewState,
        sortAndFilterDataSource: SortAndFilterDataSource
    ): ViewModelProvider.Factory
}
