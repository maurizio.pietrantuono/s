package com.pp.space.di

import com.pp.network.RetrofitClient
import com.pp.repository.MemoryRepository
import com.pp.repository.SpaceClient
import com.pp.space.utils.AndroidLogger
import com.pp.space.utils.Logger
import com.pp.usecases.MissionsListUseCase
import com.pp.usecases.MissionsListUseCase.Params
import com.pp.usecases.MissionsListUseCase.SpaceResponse
import com.pp.usecases.Repository
import com.pp.usecases.UseCase
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
abstract class ApplicationModule {

    @Binds
    abstract fun logger(androidLogger: AndroidLogger): Logger

    companion object {

        @Provides
        fun provideClient(): SpaceClient = RetrofitClient()

        @Provides
        fun provideRepository(spaceClient: SpaceClient): Repository = MemoryRepository(spaceClient)

        @Provides
        fun provideUseCase(repository: Repository): UseCase<SpaceResponse, Params> =
            MissionsListUseCase(repository, Schedulers.io(), AndroidSchedulers.mainThread())

        @Provides
        fun provideMissionsListViewModelFactoryProvider(): MissionsListViewModelFactoryProvider =
            ProductionListViewModelFactoryProvider()
    }
}
