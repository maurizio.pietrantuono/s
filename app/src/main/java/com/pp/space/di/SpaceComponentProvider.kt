package com.pp.space.di

interface SpaceComponentProvider {
    val spaceComponent: SpaceComponent
}
