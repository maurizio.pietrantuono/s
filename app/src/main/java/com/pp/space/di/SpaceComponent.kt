package com.pp.space.di

import androidx.appcompat.app.AppCompatActivity
import com.pp.space.links.LinksDialog
import com.pp.space.missionslist.view.MissionsListFragment
import com.pp.space.sortandfilter.view.SortAndFilterDialog
import dagger.BindsInstance
import dagger.Component

@MainActivityScope
@Component(modules = [SpaceModule::class], dependencies = [ApplicationComponent::class])
interface SpaceComponent {

    fun inject(missionsListFragment: MissionsListFragment)

    fun inject(sortAndFilterDialog: SortAndFilterDialog)

    fun inject(linksDialog: LinksDialog)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance activity: AppCompatActivity,
            applicationComponent: ApplicationComponent
        ): SpaceComponent
    }
}
