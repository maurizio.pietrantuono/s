package com.pp.space.di

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pp.space.missionslist.viewmodel.MissionsListViewModel
import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent
import com.pp.space.missionslist.viewmodel.SpaceViewAction
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.space.missionslist.viewmodel.mapper.Mapper
import com.pp.space.sortandfilter.viewmodel.SortAndFilterDataSource
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewModel
import com.pp.space.utils.DisposableViewModel
import com.pp.usecases.MissionsListUseCase.Params
import com.pp.usecases.MissionsListUseCase.SpaceResponse
import com.pp.usecases.UseCase
import dagger.Module
import dagger.Provides

@Module
object SpaceModule {

    @Provides
    fun provideMapper(): Function1<@JvmSuppressWildcards SpaceResponse, @JvmSuppressWildcards SpaceViewState> =
        Mapper()

    @Provides
    fun provideMissionsListViewModel(
        activity: AppCompatActivity,
        spaceUseCase: UseCase<SpaceResponse, Params>,
        mapper: (@JvmSuppressWildcards SpaceResponse) -> @JvmSuppressWildcards SpaceViewState,
        dataSource: SortAndFilterDataSource,
        missionsListViewModelFactoryProvider: MissionsListViewModelFactoryProvider
    ): DisposableViewModel<SpaceViewState, SpaceViewAction, OneOffSpaceEvent> =
        ViewModelProvider(
            activity,
            missionsListViewModelFactoryProvider.getMissionsListViewModelFactory(
                spaceUseCase,
                mapper,
                dataSource
            )
        )[MissionsListViewModel::class.java]

    @MainActivityScope
    @Provides
    fun provideSortAndFilterDataSource() = SortAndFilterDataSource()

    @Provides
    fun provideSortAndFilterViewModel(
        activity: AppCompatActivity,
        dataSource: SortAndFilterDataSource,
    ) =
        ViewModelProvider(
            activity,
            DialogViewModelFactory(dataSource)
        )[SortAndFilterViewModel::class.java]
}

private class DialogViewModelFactory(val dataSource: SortAndFilterDataSource) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        SortAndFilterViewModel(dataSource) as T
}
