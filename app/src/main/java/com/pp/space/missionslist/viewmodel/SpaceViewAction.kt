package com.pp.space.missionslist.viewmodel

import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceXLaunchesViewModel.LaunchViewModel.Links
import com.pp.usecases.SortingOrder

sealed class SpaceViewAction {

    object OnFilterClicked : SpaceViewAction()

    data class FilterApplied(
        val year: String? = null,
        val launchSuccessful: Boolean? = null,
        val sortingOrder: SortingOrder = SortingOrder.ASCENDING
    ) : SpaceViewAction()

    data class OnItemClicked(val links: Links) : SpaceViewAction()
}
