package com.pp.space.missionslist.viewmodel

import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent.OpenFilters
import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent.OpenLinksSelector
import com.pp.space.missionslist.viewmodel.SpaceViewAction.FilterApplied
import com.pp.space.missionslist.viewmodel.SpaceViewAction.OnFilterClicked
import com.pp.space.missionslist.viewmodel.SpaceViewAction.OnItemClicked
import com.pp.space.sortandfilter.viewmodel.SortAndFilterDataSource
import com.pp.space.utils.DisposableViewModel
import com.pp.usecases.MissionsListUseCase.Params
import com.pp.usecases.MissionsListUseCase.SpaceResponse
import com.pp.usecases.UseCase
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class MissionsListViewModel(
    private val spaceUseCase: UseCase<SpaceResponse, Params>,
    private val mapper: (SpaceResponse) -> SpaceViewState,
    private val sortAndFilterDataSource: SortAndFilterDataSource
) : DisposableViewModel<SpaceViewState, SpaceViewAction, OneOffSpaceEvent>() {

    private val searchParamsSubject: Subject<Params> = PublishSubject.create()

    init {
        disposable = searchParamsSubject
            .startWith(Params())
            .flatMap { params ->
                spaceUseCase.execute(params).map { data ->
                    mapper(data)
                }.startWith(SpaceViewState.Loading)
                    .doOnNext {
                        sortAndFilterDataSource.updateFilterViewState(params, it)
                    }
            }
            .subscribe(
                {
                    viewStatesSubject.onNext(it)
                },
                { /* NoOp */ }
            )
    }

    override fun accept(viewAction: SpaceViewAction) =
        when (viewAction) {
            is FilterApplied -> onFilterApplied(viewAction)
            is OnFilterClicked -> oneOffEventsSubject.onNext(OpenFilters)
            is OnItemClicked -> oneOffEventsSubject.onNext(OpenLinksSelector(viewAction.links))
        }

    private fun onFilterApplied(viewAction: FilterApplied) {
        searchParamsSubject.onNext(
            Params(
                year = viewAction.year,
                launchSuccessful = viewAction.launchSuccessful,
                sortingOrder = viewAction.sortingOrder
            )
        )
    }
}
