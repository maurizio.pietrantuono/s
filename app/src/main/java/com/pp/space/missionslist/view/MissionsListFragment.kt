package com.pp.space.missionslist.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import com.google.android.material.snackbar.Snackbar
import com.pp.space.R
import com.pp.space.databinding.FragmentSpaceBinding
import com.pp.space.di.SpaceComponentProvider
import com.pp.space.links.LinksDialog
import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent
import com.pp.space.missionslist.viewmodel.SpaceViewAction
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceXLaunchesViewModel.LaunchViewModel.Links
import com.pp.space.sortandfilter.view.SortAndFilterDialog
import com.pp.space.utils.DisposableViewModel
import com.pp.space.utils.Logger
import com.pp.space.utils.exhaustive
import com.pp.usecases.SortingOrder
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@SuppressWarnings("TooManyFunctions")
class MissionsListFragment : Fragment() {
    @Inject
    lateinit var viewModel: DisposableViewModel<SpaceViewState, SpaceViewAction, OneOffSpaceEvent>

    @Inject
    lateinit var logger: Logger
    private var compositeDisposable = CompositeDisposable()
    private lateinit var binding: FragmentSpaceBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        setFragmentResultListener(SortAndFilterDialog.REQUEST_KEY) { key, bundle ->
            val params =
                bundle.getParcelable<SortAndFilterDialog.SearchParams>(
                    SortAndFilterDialog.VIEW_STATE
                )
            viewModel.accept(
                SpaceViewAction.FilterApplied(
                    year = params?.year,
                    sortingOrder = params?.sortingOrder ?: SortingOrder.ASCENDING,
                    launchSuccessful = params?.launchSuccessful
                )
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSpaceBinding.inflate(inflater, container, false)
        (activity as SpaceComponentProvider).spaceComponent.inject(this)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        compositeDisposable.add(
            viewModel.viewStates.subscribe(
                {
                    render(it)
                },
                {
                    /* NoOp */
                }
            )
        )
        compositeDisposable.add(
            viewModel.oneOffEvents.subscribe(
                {
                    when (it) {
                        is OneOffSpaceEvent.OpenFilters -> openFiltersDialog()
                        is OneOffSpaceEvent.OpenLinksSelector -> openLinksSelector(it.links)
                    }.exhaustive
                },
                {
                    /* NoOp */
                }
            )
        )
    }

    override fun onPause() {
        super.onPause()
        compositeDisposable.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.filter_sort -> onSortClicked()
            else -> false
        }

    private fun onSortClicked(): Boolean {
        viewModel.accept(SpaceViewAction.OnFilterClicked)
        return true
    }

    private fun render(viewState: SpaceViewState) =
        when (viewState) {
            is SpaceViewState.Loading -> onLoading()
            is SpaceViewState.Data -> onData(viewState)
            is SpaceViewState.Error -> onError(viewState.message)
        }

    private fun onError(message: String) {
        binding.progress.visibility = View.GONE
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

    private fun onLoading() {
        binding.progress.visibility = View.VISIBLE
    }

    private fun onData(viewState: SpaceViewState.Data) {
        binding.progress.visibility = View.GONE
        binding.companyInfo.bind(viewState.companyViewModel)
        binding.launchesList.bind(viewState.spaceXLaunchesViewModel) {
            viewModel.accept(SpaceViewAction.OnItemClicked(it))
        }
    }

    private fun openFiltersDialog() {
        SortAndFilterDialog.newInstance().show(parentFragmentManager, SortAndFilterDialog.TAG)
    }

    private fun openLinksSelector(links: Links) {
        if (!links.isEmpty) {
            LinksDialog.newInstance(links).show(parentFragmentManager, LinksDialog.TAG)
        } else {
            logger.logException(
                MissionsListFragment::class::simpleName.toString(),
                "Cannot open links selector, no links available"
            )
        }
    }

    companion object {
        fun newInstance() = MissionsListFragment()
    }
}
