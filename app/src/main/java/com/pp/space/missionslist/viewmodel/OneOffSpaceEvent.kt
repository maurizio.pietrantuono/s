package com.pp.space.missionslist.viewmodel

sealed class OneOffSpaceEvent {

    object OpenFilters : OneOffSpaceEvent()

    data class OpenLinksSelector(val links: SpaceViewState.Data.SpaceXLaunchesViewModel.LaunchViewModel.Links) :
        OneOffSpaceEvent()
}
