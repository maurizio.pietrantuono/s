package com.pp.space.missionslist.view.recycler

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pp.space.missionslist.viewmodel.SpaceViewState

class LaunchesRecycler @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {
    private lateinit var listener: (SpaceViewState.Data.SpaceXLaunchesViewModel.LaunchViewModel.Links) -> Unit

    private val launchesAdapter: LaunchesAdapter
        get() = adapter as LaunchesAdapter

    init {
        layoutManager = LinearLayoutManager(context)
        adapter = LaunchesAdapter { links: SpaceViewState.Data.SpaceXLaunchesViewModel.LaunchViewModel.Links ->
            listener(links)
        }
    }

    fun bind(
        spaceXLaunchesViewModel: SpaceViewState.Data.SpaceXLaunchesViewModel,
        listener: (SpaceViewState.Data.SpaceXLaunchesViewModel.LaunchViewModel.Links) -> Unit
    ) {
        launchesAdapter.registerAdapterDataObserver(object : AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                if (itemCount != spaceXLaunchesViewModel.launchViewModels.size) {
                    scrollToPosition(0)
                }
            }

            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                scrollToPosition(0)
            }

            override fun onItemRangeMoved(
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
                scrollToPosition(0)
            }
        })
        launchesAdapter.submitList(spaceXLaunchesViewModel.launchViewModels)
        this.listener = listener
    }
}
