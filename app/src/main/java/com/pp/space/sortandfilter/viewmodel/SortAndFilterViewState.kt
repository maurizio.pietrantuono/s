package com.pp.space.sortandfilter.viewmodel

import android.os.Parcelable
import com.pp.usecases.SortingOrder
import kotlinx.parcelize.Parcelize

@Parcelize
data class SortAndFilterViewState(
    val year: String? = null,
    val launchSuccessful: Boolean? = null,
    val sortingOrder: SortingOrder = SortingOrder.ASCENDING,
    val years: List<String> = emptyList()
) : Parcelable
