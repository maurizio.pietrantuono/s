package com.pp.space.sortandfilter.viewmodel

import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.usecases.MissionsListUseCase
import io.reactivex.subjects.BehaviorSubject

class SortAndFilterDataSource {
    internal val sortAndFilterViewStateSubject: BehaviorSubject<SortAndFilterViewState> =
        BehaviorSubject.createDefault(SortAndFilterViewState())

    internal fun updateFilterViewState(
        params: MissionsListUseCase.Params,
        spaceViewState: SpaceViewState
    ) {
        val filterViewState = requireNotNull(sortAndFilterViewStateSubject.value)
        sortAndFilterViewStateSubject.onNext(
            filterViewState.copy(
                year = params.year,
                launchSuccessful = params.launchSuccessful,
                sortingOrder = params.sortingOrder,
                years = if (spaceViewState is SpaceViewState.Data && filterViewState.years.isEmpty()) {
                    val launches = spaceViewState.spaceXLaunchesViewModel.launchViewModels
                    listOf("All") + launches.map { it.year }.distinct()
                } else {
                    filterViewState.years
                }
            )
        )
    }
}
