package com.pp.space.sortandfilter.viewmodel

import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnSortingOrderChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnSuccessChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnYearChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.ResetState
import com.pp.space.utils.DisposableViewModel
import io.reactivex.Observable

class SortAndFilterViewModel(private val dataSource: SortAndFilterDataSource) :
    DisposableViewModel<SortAndFilterViewState, SortAndFilterViewAction, Nothing>() {

    override val viewStates: Observable<SortAndFilterViewState>
        get() = Observable.just(state)

    private var state: SortAndFilterViewState =
        requireNotNull(dataSource.sortAndFilterViewStateSubject.value)

    override fun accept(viewAction: SortAndFilterViewAction) {
        state = when (viewAction) {
            is OnSuccessChanged -> state.copy(launchSuccessful = viewAction.successful)
            is OnSortingOrderChanged -> state.copy(sortingOrder = viewAction.sortingOrder)
            is OnYearChanged -> state.copy(year = viewAction.year)
            is ResetState -> requireNotNull(dataSource.sortAndFilterViewStateSubject.value)
        }
    }
}
