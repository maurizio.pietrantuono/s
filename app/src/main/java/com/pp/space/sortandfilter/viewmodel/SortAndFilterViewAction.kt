package com.pp.space.sortandfilter.viewmodel

import com.pp.usecases.SortingOrder

sealed class SortAndFilterViewAction {
    data class OnSuccessChanged(val successful: Boolean?) : SortAndFilterViewAction()
    data class OnSortingOrderChanged(val sortingOrder: SortingOrder) : SortAndFilterViewAction()
    data class OnYearChanged(val year: String?) : SortAndFilterViewAction()
    object ResetState : SortAndFilterViewAction()
}
