package com.pp.space.missionslist.viewmodel

import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent.OpenFilters
import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent.OpenLinksSelector
import com.pp.space.missionslist.viewmodel.SpaceViewAction.FilterApplied
import com.pp.space.missionslist.viewmodel.SpaceViewAction.OnFilterClicked
import com.pp.space.missionslist.viewmodel.SpaceViewAction.OnItemClicked
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceXLaunchesViewModel.LaunchViewModel.Links
import com.pp.space.missionslist.viewmodel.SpaceViewState.Error
import com.pp.space.missionslist.viewmodel.SpaceViewState.Loading
import com.pp.space.missionslist.viewmodel.mapper.Mapper
import com.pp.space.sortandfilter.viewmodel.SortAndFilterDataSource
import com.pp.usecases.MissionsListUseCase.Params
import com.pp.usecases.MissionsListUseCase.SpaceResponse
import com.pp.usecases.UseCase
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class MissionsListViewModelTest {
    private val links: Links = mockk(relaxed = true)
    private lateinit var viewModel: MissionsListViewModel
    private val dataResponse: SpaceResponse.Data = mockk(relaxed = true)
    private val spaceUseCase: UseCase<SpaceResponse, Params> = mockk(relaxed = true)
    private val mapper: (SpaceResponse) -> SpaceViewState = Mapper()
    private val sortAndFilterDataSource: SortAndFilterDataSource = mockk(relaxed = true)

    @Before
    fun setUp() {
        every { spaceUseCase.execute(any()) } returns Observable.just(dataResponse)
        viewModel = MissionsListViewModel(spaceUseCase, mapper, sortAndFilterDataSource)
    }

    @Test
    fun `when starts then emits loading`() {
        every { spaceUseCase.execute(any()) } returns Observable.empty()
        viewModel = MissionsListViewModel(spaceUseCase, mapper, sortAndFilterDataSource)
        val testObserver = viewModel.viewStates.test()

        val result = testObserver.values().first()
        assertThat(result).isEqualTo(Loading)
    }

    @Test
    fun `when use case returns a response then emits view state`() {
        val testObserver = viewModel.viewStates.test()

        val result = testObserver.values().first()
        assertThat(result).isExactlyInstanceOf(Data::class.java)
    }

    @Test
    fun `when use case returns a error then emits error`() {
        every { spaceUseCase.execute(any()) } returns Observable.just(SpaceResponse.Error(MESSAGE))
        viewModel = MissionsListViewModel(spaceUseCase, mapper, sortAndFilterDataSource)

        val testObserver = viewModel.viewStates.test()

        testObserver.assertValue(Error(MESSAGE))
    }

    @Test
    fun `when receives OnFilterClicked then emits OpenFilters`() {
        val testObserver = viewModel.oneOffEvents.test()

        viewModel.accept(OnFilterClicked)

        testObserver.assertValue(OpenFilters)
    }

    @Test
    fun `when receives FilterApplied then calls use case`() {
        viewModel.accept(FilterApplied(year = YEAR))

        verify { spaceUseCase.execute(Params(year = YEAR)) }
    }

    @Test
    fun `when receives OnItemClicked then emits OpenLinksSelector`() {
        val testObserver = viewModel.oneOffEvents.test()

        viewModel.accept(OnItemClicked(links))

        testObserver.assertValue(OpenLinksSelector(links))
    }

    private companion object {
        private const val MESSAGE = "message"
        private const val YEAR = "year"
    }
}
