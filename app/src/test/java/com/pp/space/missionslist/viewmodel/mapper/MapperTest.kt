package com.pp.space.missionslist.viewmodel.mapper

import com.pp.CompanyModel
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceXCompanyViewModel
import com.pp.usecases.MissionsListUseCase.SpaceResponse.Data
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class MapperTest { // TODO add more tests
    private val timeMapper: TimeMapper = mockk(relaxed = true)
    private val mapper = Mapper(timeMapper)

    @Test
    fun `when maps data then maps it correctly`() {
        val result = mapper(data)

        assertThat(result).isEqualTo(spaceViewStateData)
    }

    companion object {
        private const val ELON = "Elon"
        private const val EMPTY_STRING = ""
        private const val ONE = 1
        private const val ONE_STRING = "1"

        private val data = Data(
            companyInfo = CompanyModel(
                founder = ELON,
                employees = ONE,
                founded = ONE_STRING,
                name = EMPTY_STRING,
                valuation = ONE.toLong(),
                launchSites = ONE
            ),
            launches = emptyList()
        )

        private val spaceViewStateData = SpaceViewState.Data(
            companyViewModel = SpaceXCompanyViewModel(
                companyName = EMPTY_STRING,
                founderName = ELON,
                employeesNumber = ONE_STRING,
                launchSitesNumber = ONE,
                valuation = ONE_STRING,
                yearFounded = ONE_STRING
            ),
            spaceXLaunchesViewModel = SpaceViewState.Data.SpaceXLaunchesViewModel(emptyList())
        )
    }
}
