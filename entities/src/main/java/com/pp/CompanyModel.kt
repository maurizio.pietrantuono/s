package com.pp

data class CompanyModel(
    val name: String,
    val founder: String,
    val employees: Int,
    val launchSites: Int,
    val valuation: Long,
    val founded: String
)
